function domID(id){
    return document.getElementById(id);
}
function tinhThuNhapChiuThue(a,b){
    var c = a-(4e+6)-b*(1600000);
    return c;
}
function tinhThue(a,b){
    var tienThue = a*b;
    return tienThue;
}
domID('tinhTien').onclick= function(){
    var hoTen = domID('txt-ho-ten').value;
    var thuNhapNam = domID("txt-thu-nhap").value*1;
    var ngPhuThuoc = domID('txt-nguoi-phu-thuoc').value*1;
    var thuNhapChiuThue = tinhThuNhapChiuThue(thuNhapNam,ngPhuThuoc);
    var nguongThuNhap = thuNhapChiuThue/1000000;
    var tienThue=0;
    if(nguongThuNhap<=1||ngPhuThuoc <=0){
        alert('dữ liệu không hợp lệ');

    } else if (nguongThuNhap<=60){
        tienThue = tinhThue(thuNhapChiuThue,0.05)
    }
     else if (nguongThuNhap<=120){
        tienThue = tinhThue(thuNhapChiuThue,0.1)
    }
     else if (nguongThuNhap<=210){
        tienThue = tinhThue(thuNhapChiuThue,0.15)
    }
     else if (nguongThuNhap<=384){
        tienThue = tinhThue(thuNhapChiuThue,0.2)
    }
     else if (nguongThuNhap<=624){
        tienThue = tinhThue(thuNhapChiuThue,0.25)
    }
     else if (nguongThuNhap<=960){
        tienThue = tinhThue(thuNhapChiuThue,0.3)
    }
     else {
        tienThue = tinhThue(thuNhapChiuThue,0.35)
    }
    domID('result').innerHTML = `họ và tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${(new Intl.NumberFormat().format(tienThue))} VND.`
}
